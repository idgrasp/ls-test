/**
 * @file      include/common.h
 * @author    Ivan Dmitriev <ivan.dmitriev92@gmail.com>
 * @brief     Common file for the ls-test app.
 *
 * @{
 */
#pragma once

#include <config.h>

#ifdef CONFIG_WITH_COLORS
#define DIRECTORY_COLOR  "\x1B[36m"
#define LINK_COLOR       "\x1B[32m"
#define FILE_COLOR       "\x1B[0m"

#define ENDCOLOR         "\x1B[0m"
#else
#define DIRECTORY_COLOR
#define LINK_COLOR
#define FILE_COLOR
#define ENDCOLOR
#endif /* CONFIG_WITH_COLORS */

/**
 * @brief Output formats.
 */
typedef enum {
    LF_SIMPLE       = 0,
    LF_LONG_LIST,                   /**< Only filenames. */
    LF_DEFAULT      = LF_SIMPLE,    /**< List with the attributes. */
} list_format_t;

/** @} */
