# ls-test

Interview test.

## Build

`$ mkdir build && cd build`

`$ cmake .. -DCMAKE_BUILD_TYPE=RELEASE -DCONFIG_WITH_COLORS=TRUE`

`$ make`

## Run

`$ ./ls-test --help`

`$ ./ls-test -l /tmp`

`$ ./ls-test`

## Any questions?

ivan.dmitriev92@gmail.com
