/**
 * @file      src/main.c
 * @author    Ivan Dmitriev <ivan.dmitriev92@gmail.com>
 * @brief     A test program which acts almost like a system `ls` utilty. Only `-a` and `-l` flags
 *            are supported.
 */

#include <common.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <limits.h>

typedef struct {
    bool list;      /**< `-l` option. Long listing format. */
    bool all;       /**< -a option. Do not ignore entries starting with `.` */
    const char *dir;
} app_arguments_t;

static struct option app_options[] = {
    {.name = "help", .val = 'h'},
    {.name = "list", .val = 'l'},
    {.name = "all" , .val = 'a'},
    {.name = NULL} /* terminator */
};

static void
_help(void)
{
    printf(
        "\n"
        "Usage:\n"
        "  "BINARY_NAME" [OPTIONS] PATH\n"
        "\n"
        "Options:\n"
        "  -h, --help             Show help and exit\n"
        "  -l                     Long list format\n"
        "  -a                     Do not ignore entries starting with .\n"
        "\n"
        "Report bugs to <ivan.dmitriev92@gmail.com>\n"
    );
} /* end of _help */

static void
_parse_args(app_arguments_t *args, int argc, char *argv[])
{
    assert(args != NULL && argv != NULL);

    memset(args, 0, sizeof(*args));
    int opt;

    while ((opt = getopt_long(argc, argv, "hla", app_options, NULL)) != -1) {
        switch (opt) {
        case 'h' :            /* --help */
            _help();
            exit(EXIT_SUCCESS);
            break;
        case 'l' :            /**/
            args->list = true;
            break;
        case 'a' :            /* --all */
            args->all = true;
            break;
        default :
            _help();
            exit(EXIT_FAILURE);
            break;
        }
    }
    if (optind < argc) {
        args->dir = argv[optind];
    }
} /* end of _parse_args */

/**
 * @brief Excludes files with `.` prefix.
 */
static int
hidden_filter(const struct dirent *entry)
{
    return entry->d_name[0] != '.';
} /* end of hidden_filter */

static inline int
process_dir_entries(
    struct dirent *const *entries, size_t len, DIR *dir, list_format_t format)
{
    if (!entries) {
        return -1;
    }
    switch (format) {
    case LF_SIMPLE :
        for (uint i = 0; i < len; i++) {
            switch (entries[i]->d_type) {
            case DT_DIR :
                printf(DIRECTORY_COLOR"%s\n"ENDCOLOR, entries[i]->d_name);
                break;
            case DT_LNK :
                printf(DIRECTORY_COLOR"%s\n"ENDCOLOR, entries[i]->d_name);
                break;
            default :
                printf(FILE_COLOR"%s\n"ENDCOLOR, entries[i]->d_name);
                break;

            }
        }
        break;
    case LF_LONG_LIST : {
        /* NOTE: we do not print total blocks at first line here. */
        if (!dir) {
            return -1;
        }
        time_t now = time(NULL);
        struct tm now_tm;
        if (!localtime_r(&now, &now_tm)) {
            now_tm.tm_year = INT_MAX;
        }
        int dird = dirfd(dir);
        struct stat statbuf;

        for (uint i = 0; i < len; i++) {
            const struct dirent *e = entries[i];
            if (fstatat(dird, e->d_name, &statbuf, AT_SYMLINK_NOFOLLOW)) {
                perror("fstatat()");
                continue;
            }
            /* Print file type: */
            switch (e->d_type) {
            case DT_DIR :
                printf("d");
                break;
            case DT_LNK :
                printf("l");
                break;
            default :
                printf("-");
                break;

            }
            /* Print access rights: */
#define _P(_mode, _perm, _text, _empty) ((_mode & _perm) == _perm ? _text : _empty)
#define P(_perm, _text) _P(statbuf.st_mode, _perm, _text, "-")

#define PRIperm "%s%s%s"
#define PERM(_group) P(S_IR##_group, "r"), P(S_IW##_group, "w"), P(S_IX##_group, "x")

            printf(PRIperm PRIperm PRIperm " ", PERM(USR), PERM(GRP), PERM(OTH));

            /* Print number of links: */
            printf("%2ld ", statbuf.st_nlink);

            /* Print user name: */
            const struct passwd *pw = getpwuid(statbuf.st_uid);
            if (pw) {
                printf("%-8s ", pw->pw_name);
            } else {
                /* Print user ID if user was not found. */
                printf("%-8d ", statbuf.st_uid);
            }

            /* Print group name: */
            const struct group *grp = getgrgid(statbuf.st_gid);
            if (grp) {
                printf("%-8s ", grp->gr_name);
            } else {
                /* Print user ID if user was not found. */
                printf("%-8d ", statbuf.st_gid);
            }

            /* Print size: */
            printf("%9ld ", statbuf.st_size);

            /* Print last modification date: */
            struct tm modify_tm;
            char time_buf[32];

            if (!localtime_r(&statbuf.st_mtim.tv_sec, &modify_tm)) {
                perror("localtime_r()");
                sprintf(time_buf, "<unknown>");
            } else {
                if (modify_tm.tm_year < now_tm.tm_year) {
                    /* Print year instead of time if file is too old: */
                    strftime(time_buf, sizeof(time_buf), "%b %e  %Y", &modify_tm);
                } else {
                    strftime(time_buf, sizeof(time_buf), "%b %e %R", &modify_tm);
                }
            }
            printf("%-12s ", time_buf);

            /* Print file name: */
#ifdef CONFIG_WITH_COLORS
            switch (e->d_type) {
            case DT_DIR :
                printf(DIRECTORY_COLOR);
                break;
            case DT_LNK :
                printf(LINK_COLOR);
                break;
            default :
                printf(FILE_COLOR);
                break;
            }
#endif
            printf("%s\n"ENDCOLOR, e->d_name);
        }
        break; }
    }
    return 0;
} /* end of process_dir_entries */

static inline void
free_entries(struct dirent **entries, size_t n)
{
    for (uint i = 0; i < n; i++) {
        free(entries[i]);
    }
    free(entries);
} /* end of free_entries */

int
main(int argc, char *argv[])
{
    /* Parse CLI arguments: */
    app_arguments_t args;
    _parse_args(&args, argc, argv);

    struct dirent **entries = NULL;

    const char *path = args.dir ? args.dir : ".";

    int n = scandir(path, &entries, args.all ? NULL : hidden_filter, alphasort);
    if (n == -1) {
        perror("scandir()");
        exit(EXIT_FAILURE);
    }

    DIR *dir = NULL;
    if (args.list == LF_LONG_LIST) {
        dir = opendir(path);
        if (!dir) {
            free_entries(entries, n);
            perror("opendir()");
            exit(EXIT_FAILURE);
        }
    }

    if (process_dir_entries(entries, n, dir, args.list ? LF_LONG_LIST : LF_DEFAULT)) {
        /* Error occured. */
    }
    if (dir) {
        closedir(dir);
    }
    /* Free entries data: */
    free_entries(entries, n);

    return EXIT_SUCCESS;
} /* end of main */
